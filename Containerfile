FROM gcr.io/google.com/cloudsdktool/google-cloud-cli:alpine
# based on https://github.com/alpine-docker/helm/blob/master/Dockerfile

ARG HELM_VERSION
ARG KUBECTL_VERSION

ENV HELM_BASE_URL="https://get.helm.sh"
ENV KUBECTL_BASE_URL="https://dl.k8s.io/release"

RUN case `uname -m` in \
        x86_64) ARCH=amd64; ;; \
        armv7l) ARCH=arm; ;; \
        aarch64) ARCH=arm64; ;; \
        *) echo "un-supported arch, exit ..."; exit 1; ;; \
    esac && \
    wget --no-verbose ${HELM_BASE_URL}/helm-v${HELM_VERSION}-linux-${ARCH}.tar.gz -O - | tar -xz && \
    mv linux-${ARCH}/helm /usr/bin/helm && \
    rm -rf linux-${ARCH} && \
    if [ -z "${KUBECTL_VERSION}" ] ; then KUBECTL_VERSION=$(curl -L -s ${KUBECTL_BASE_URL}/stable.txt | cut -c 2-) ; fi && \
    wget --no-verbose ${KUBECTL_BASE_URL}/v${KUBECTL_VERSION}/bin/linux/${ARCH}/kubectl -O /usr/bin/kubectl && \
    chmod +x /usr/bin/helm /usr/bin/kubectl && \
    gcloud components install -q gke-gcloud-auth-plugin && \
    rm -rf $(find google-cloud-sdk/ -regex ".*/__pycache__") && \
    rm -rf google-cloud-sdk/.install/.backup
