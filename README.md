# Helm gcloud by DMI Commerce DevOps Team
The main purpose of this image is to build Helm images with the latest gcloud
cli. The Helm version is defined in the environment variable HELM_VERSION.
Secondarily, we can specify the kubectl version by setting the environment variable
KUBECTL_VERSION, if this variable is empty the latest one will be downloaded.

If we want to mark a Helm version as the latest, we have to set the
variable TAG_LATEST to true.

## Image creation
### Scenario #1: Create the image with a specific Helm version and latest gcloud and kubectl
* Edit the variable HELM_VERSION to the desired one
* Trigger manually the pipeline

### Scenario #2: Create the image with a specific Helm version and latest gcloud and kubectl. Tag image as latest
* Trigger manually the pipeline setting the following variables
  * HELM_VERSION=3.9.4
  * TAG_LATEST=true

### Scenario #3: Create image with fixed version of Helm and a fixed kubectl
* Trigger manually the pipeline setting the following variables
  * HELM_VERSION=3.9.4
  * KUBECTL_VERSION=1.25.0
